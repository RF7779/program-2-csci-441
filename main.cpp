#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"

//Please note that GLM is used, but only for file loading, since LITERALLY EVERY RESOURSE I COULD FIND DID SO THROUGH GLM.
//Rverything else in the entire project is GLM-less.
#include <glm/glm.hpp>
//#include "objloader.cpp"	//whoops
#include "objloader.hpp"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;
int cameraMode = 1;
int wCount = 0;
Camera camera; //not needed

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
	return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
	return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window, Camera& currentCamera, Camera& camera, Camera& altCamera) {
	Matrix trans;

	const float ROT = 1;
	const float SCALE = .05;
	const float TRANS = .01;

	// ROTATE
	//if (isPressed(window, GLFW_KEY_F)) { trans.rotate_x(-ROT); }		//Yeah, probably never going work
	//else if (isPressed(window, GLFW_KEY_R)) { trans.rotate_x(ROT); }
	//else if (isPressed(window, GLFW_KEY_E)) { trans.rotate_y(-SCALE); }		//NEEDS CALIBRATION
	//else if (isPressed(window, GLFW_KEY_Q)) { trans.rotate_y(SCALE); }
	if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
	else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
	// SCALE
	else if (isPressed(window, '-')) { trans.scale(1 - SCALE, 1 - SCALE, 1 - SCALE); }
	else if (isPressed(window, '=')) { trans.scale(1 + SCALE, 1 + SCALE, 1 + SCALE); }
	// TRANSLATE
	else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
	else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
	else if (isPressed(window, GLFW_KEY_A)) { trans.translate(-SCALE, 0, 0); }		//Scale used here to match the arbitrarily not-snail-slow speed used for camera movement below
	else if (isPressed(window, GLFW_KEY_D )) { trans.translate(SCALE, 0, 0); }
	else if (isPressed(window, GLFW_KEY_S)) { trans.translate(0, 0, SCALE); }
	else if (isPressed(window, GLFW_KEY_W)) { trans.translate(0, 0, -SCALE); }
	//camera.eye.values[2] = camera.eye.values[2] + 1;
	return trans * model;
}

//reminder:
//camera.eye = Vector(1, 0, 3);
//camera.origin = Vector(0, 0, 0);
//camera.up = Vector(0, 1, 0);

void processInput(Matrix& model, GLFWwindow *window, Camera& currentCamera, Camera& camera, Camera& altCamera) {
	Matrix trans;
	if (isPressed(window, GLFW_KEY_C) || isPressed(window, GLFW_KEY_SPACE)) {	//Switching between character and Bird's eye cameras
		if (cameraMode == 1) {
			currentCamera = altCamera;
			cameraMode = 2;
			Sleep(150);		//Makes camera transitions less seizure inducing and/or horror-game emulating.
		}
		else if (cameraMode == 2) {
			cameraMode = 1;
			currentCamera = camera;
			Sleep(150);
			//std::cerr << "camera transition" << std::endl;
		}
	}
	//if (cameraMode == 1) {		//If we are in "Character mode" camera
		if (isPressed(window, GLFW_KEY_ESCAPE)) {
			glfwSetWindowShouldClose(window, true);
		}
		/*		//Ambitious, but it doesn't support simultaionus key presses for diagonal translations, explicit or not.
		else if (isPressed(window, GLFW_KEY_W) && isPressed(window, GLFW_KEY_D)) {
			camera.eye.values[2] = ((camera.eye.values[2]) - .05);	//MOVE CAMERA FORWARD
			camera.origin.values[2] = ((camera.origin.values[2]) - .05);
			camera.eye.values[0] = ((camera.eye.values[0]) + .05); // CAMERA STRAFE RIGHT
			camera.origin.values[0] = ((camera.origin.values[0]) + .05);
		}
		*/
		else if (isPressed(window, GLFW_KEY_W)) {
				wCount++;
				//camera.eye = Vector(1+ wCount, 0, 3);
				//camera.eye = camera.eye * trans.translate(0, .01, 0);	//doesn't work that way

				/*
				//Almost there, not quite:
				camera.eye.values[0] = camera.eye.values[0] + 50;
				camera.origin.values[0] += 50;
				//trans.translate(0, 1, 0);
				projection = projection * trans;
				//projection = camera.look_at();	//MAybe it's just the placement of this line?
				//std::cerr << "w line occured" << std::endl;	//debugging line. Occures reliably.
				camera.eye = Vector(1, 0, 10);
				camera.look_at();
				camera.eye.values[2] = ((camera.eye.values[2]) + 1);
				std::cerr << camera.eye.values[2] << std::endl; //okay. here's our problem. The value isn't actually changing. We just keep printing out the same int but never actually update the camera
				*/

				//Turns out all this trial and error (for not) was because I forgot "&" after "Camera" in the inputs. That was like two hours of work for a single character mistake
				//WHOOPS.
				camera.eye.values[2] = ((camera.eye.values[2]) - .05);	//MOVE CAMERA FORWARD
				camera.origin.values[2] = ((camera.origin.values[2]) - .05);	//Keeps the camera "forward" as we strafe
		}
		else if (isPressed(window, GLFW_KEY_S)) {
				camera.eye.values[2] = ((camera.eye.values[2]) + .05); // CAMERA BACKWARD
				camera.origin.values[2] = ((camera.origin.values[2]) + .05);
		}
		else if (isPressed(window, GLFW_KEY_Q)) {
				camera.origin.values[0] = ((camera.origin.values[0]) - .05);	//CAMERA PAN LEFT
		}
		else if (isPressed(window, GLFW_KEY_E)) {
				camera.origin.values[0] = ((camera.origin.values[0]) + .05);	//CAMERA PAN RIGHT
		}
		else if (isPressed(window, GLFW_KEY_R)) {
				camera.origin.values[1] = ((camera.origin.values[1]) + .05);	//CAMERA ANGLE UP
		}
		else if (isPressed(window, GLFW_KEY_F)) {
				camera.origin.values[1] = ((camera.origin.values[1]) - .05);	//CAMERA ANGLE DOWN
		}
		else if (isPressed(window, GLFW_KEY_A)) {
				camera.eye.values[0] = ((camera.eye.values[0]) - .05); // CAMERA STRAFE LEFT
				camera.origin.values[0] = ((camera.origin.values[0]) - .05);	//Keeps the camera "forward" as we strafe
				//trans = trans.translate(-.05, 0, 0);
				//else if (isPressed(window, GLFW_KEY_D)) { trans.translate(TRANS, 0, 0); }
		}
		else if (isPressed(window, GLFW_KEY_D)) {
				camera.eye.values[0] = ((camera.eye.values[0]) + .05); // CAMERA STRAFE RIGHT
				camera.origin.values[0] = ((camera.origin.values[0]) + .05);
		}
		if (cameraMode == 1) {
			currentCamera = camera;
		}
	//}
	model = processModel(model, window, currentCamera, camera, altCamera);
}

void errorCallback(int error, const char* description) {
	fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
	GLFWwindow* window;

	glfwSetErrorCallback(errorCallback);

	/* Initialize the library */
	if (!glfwInit()) { return -1; }

	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	// tell glfw what to do on resize
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

	// init glad
	if (!gladLoadGL()) {
		std::cerr << "Failed to initialize OpenGL context" << std::endl;
		glfwTerminate();
		return -1;
	}

	// create obj
	Model obj(
		Torus(40, .75, .5, 1, .2, .4).coords,
		Shader("C:/Users/Ryan/source/repos/GraphicsProgram2/GraphicsProgram2/vert.glsl", "C:/Users/Ryan/source/repos/GraphicsProgram2/GraphicsProgram2/frag.glsl"));	//********REPLACE THIS LINE WITH THE DIRECTORY RELEVANT TO YOUR SYSTEM********
	Matrix obj_trans, obj_scale;
	obj_trans.translate(0, -1, +2.2);
	obj_scale.scale(.2, .2, .2);
	obj.model = obj_trans * obj_scale;
																						// make a floor
	Model floor(
		DiscoCube().coords,
		Shader("C:/Users/Ryan/source/repos/GraphicsProgram2/GraphicsProgram2/vert.glsl", "C:/Users/Ryan/source/repos/GraphicsProgram2/GraphicsProgram2/frag.glsl")); //********REPLACE THIS LINE WITH THE DIRECTORY RELEVANT TO YOUR SYSTEM********
	Matrix floor_trans, floor_scale;
	floor_trans.translate(0, -.5, -6);
	floor_scale.scale(2, 1, 2);
	floor.model = floor_trans * floor_scale;

	//attempts at importing, recorded for posterity
	/*
	Model maze(		
		maze.loadObj(Maze.obj)
	)
	*/
	/*
	Model maze;
	maze.loadObj(Maze.obj);
	*/
	//It was at this point I realized every tutorial I had found was using files that used GLM for this.
	//Whoops.
	//Well, I genuinely hope this means "less extra credit awarded" than, say, "an automatic failure."
	//I mean, I'd be happy to submit this exact same code, again, without the code I used to import files if that was the case.
	//As cited in the objloader ccp and hpp files, the primary resourse for this was http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/
	//and the two repositories cited therein.
	std::vector< glm::vec3 > vertices;
	std::vector< glm::vec2 > uvs;
	std::vector< glm::vec3 > normals;
	bool ohNo = loadOBJ("Maze.obj", vertices, uvs, normals);

	//A few things that didn't work, in conjunction with others that were tampered with/deleted.
	//glBindVertexArray(VAO1);
	//GLuint VBO[500];		
	//glGenBuffers(0, VBO);	
	//glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
	//glDrawArrays(GL_TRIANGLES, 0, vertices.size() * sizeof(float));
	//Model maze(vertices, Shader("C:/Users/Ryan/source/repos/GraphicsProgram2/GraphicsProgram2/vert.glsl", "C:/Users/Ryan/source/repos/GraphicsProgram2/GraphicsProgram2/frag.glsl"));

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);	//our data is loaded

	GLuint leBuffer; //It's a french buffer. Or would that be 'Le Buffour?'
	glGenBuffers(1, &leBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, leBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	GLuint myOtherBufferIsFilledWithVertices;
	glGenBuffers(1, &myOtherBufferIsFilledWithVertices);
	glBindBuffer(GL_ARRAY_BUFFER, myOtherBufferIsFilledWithVertices);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	



	//attempting to load in our character:
	std::vector< glm::vec3 > vertices2;
	std::vector< glm::vec2 > uvs2;
	std::vector< glm::vec3 > normals2;
	bool ohNo2 = loadOBJ("toyHunter.obj", vertices2, uvs2, normals2);
	glBufferData(GL_ARRAY_BUFFER, vertices2.size() * sizeof(glm::vec3), &vertices2[0], GL_STATIC_DRAW);	//our data is loaded

	GLuint charVerts; 
	glGenBuffers(1, &charVerts);
	glBindBuffer(GL_ARRAY_BUFFER, charVerts);
	glBufferData(GL_ARRAY_BUFFER, vertices2.size() * sizeof(glm::vec3), &vertices2[0], GL_STATIC_DRAW);

	GLuint charShader;
	glGenBuffers(1, &charShader);
	glBindBuffer(GL_ARRAY_BUFFER, charShader);
	glBufferData(GL_ARRAY_BUFFER, uvs2.size() * sizeof(glm::vec2), &uvs2[0], GL_STATIC_DRAW);






	// setup camera
	Matrix projection;
	projection.perspective(45, 1, .01, 10);

	Camera camera;
	camera.projection = projection;		//eventually need to center this in the taurus. Done.
	camera.eye = Vector(0, 0, +2);		//shifted two with the starting pos of the taurus
	camera.origin = Vector(0, 0, -1);
	camera.up = Vector(0, 1, 0);

	Camera altCamera;		//Birdeye
	altCamera.projection = projection;		
	altCamera.eye = Vector(0, 6.8, -5);
	altCamera.origin = Vector(0, 0, -5);
	altCamera.up = Vector(0, 0, -1);

	Camera currentCamera = camera;

	// and use z-buffering
	glEnable(GL_DEPTH_TEST);

	// create a renderer
	Renderer renderer;

	// set the light position
	Vector lightPos(3.75f, 3.75f, 4.0f);


	//do {
		//Now to draw the stored info
		//glUseProgram(Shader("C:/Users/Ryan/source/repos/GraphicsProgram2/GraphicsProgram2/vert.glsl", "C:/Users/Ryan/source/repos/GraphicsProgram2/GraphicsProgram2/frag.glsl"));
/*
		glActiveTexture(GL_TEXTURE0);
		//glBindTexture(GL_TEXTURE_2D, Texture);
		
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, leBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, myOtherBufferIsFilledWithVertices);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		
		glDrawArrays(GL_TRIANGLES, 0, vertices.size());
*/
		//glDisableVertexAttribArray(0);
		//glDisableVertexAttribArray(1);
	//}

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window)) {		//do while format messing this up. This semi colon breaks the entire thing
		// process input
		processInput(obj.model, window, currentCamera, camera, altCamera);
		//Myriad of failed rendering attempts kept for posterity.

		/* Render here */
		//renderer.render(currentCamera, obj, lightPos);


		//glActiveTexture(GL_TEXTURE0);
		//glBindTexture(GL_TEXTURE_2D, Texture);

		/*
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, leBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		*/
		/*
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, myOtherBufferIsFilledWithVertices);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

		glDrawArrays(GL_TRIANGLES, 0, vertices.size());
		*/
		//glDisableVertexAttribArray(0);
		//glDisableVertexAttribArray(1);
		//glfwSwapBuffers(window);

		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// render the object and the floor
		//std::cerr << camera.eye.values[2] << std::endl;	//debugging line
		//camera.eye.values[2]++;	//testline


		//Attempt to load the character:
		
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, charVerts);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);	
		
		renderer.render(currentCamera, obj, lightPos);
		
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);	
		glDrawArrays(GL_TRIANGLES, 0, vertices.size());
		


		//"working" maze portion:
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, leBuffer);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);	//***this line is causing problems. A GLuint index[first attribute of this method] of 0 displays our maze. Using any other value gives us our torus
		glDrawArrays(GL_TRIANGLES, 0, vertices.size());

		renderer.render(currentCamera, floor, lightPos);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);	//***this line is causing problems. A GLuint index[first attribute of this method] of 0 displays our maze. Using any other value gives us our torus
		glDrawArrays(GL_TRIANGLES, 0, vertices.size());

		//glBindVertexArray(leBuffer);
		//glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		//glDrawArrays(GL_TRIANGLES, 0, vertices.size());

		//renderer.render(currentCamera, floor, lightPos);
		//renderer.render(currentCamera, maze, lightPos);


		/* Swap front and back and poll for io events */
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}