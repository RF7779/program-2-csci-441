**Please be sure to add the toyHunter.obj file submitted to the D2L dropbox to the directory you attempt to run this repository from! The file refused to be uploaded here in the source!**
Also please be mindful to update lines 202 and 210 to match the file paths to the vert and frag files as they are on your machine.

The raw readme will probably display better.
Be sure to update the Shader lines pathing to be correct to their location on your system. I mean you probably knew to do that already but you wouldn't beleive how long it took me to figure that out.

Quickstart:
	W		
A	D	F	--	Directional movement of Character and Character Camera
----------------------------------------------
Q		E	--	Panning Left and Right of Character Camera
----------------------------------------------
R			-- Panning Up and Down of Character Camera. Also my initials.
F			
----------------------------------------------
C or Spacebar	--	Toggles camera mode
----------------------------------------------

Unfortunately, dispite my best efforts explicit and implicit simultaious commands do not currently work.
E.G. You cannot press both "w" and "f" to move diagonally. Attempting to use any combination of commands (except C or Space + another command) will break the camera from the actor.
I'm going to say this was actually left in as a feature so that you can back away from the character model and view it from the character camera.

Still implementing a function for a the player's flashlight for the *spooky* part of the maze.
Restrictive look left/right controls are intentional to encourage the player to use/discover the birdeye camera to navigate back towards the exit from the far wall (not really, still  figuring out the math of when to transition from looking close to +/- infinty X to switching the camera orgin to a negative Z to start looking behind you, but that has not been a trivial problem. Espeically since I don't want to drag in sin/cos into the matter).

Update:
Rather than fix the camera, which technically met the program's requirements, I focused on getting a character import to work, as well as the shaders, as those were yet to be accomplished at all.
The good news it would appear I found the issue to both is simply a mis-step in my parsing of the file and either the read-in of normals or applying the shader to them.
Now, while this is forward progress, the use of the more up-to-date "toyMaze.obj" file arguably makes the maze less recognizeable (atleast from the birdeye camera), although, in the broadest sense, it does in fact render "more correctly."
But it's still broke.
Likewise our character import is """working""" now. The camera is centered around waist height. If you look down you can see your feet!

If you really feel like going the extra mile to get the full experience of this slow motion trainwreck:
you can compare the two maze instantiations by changing line 235 file name between "Maze.obj" and "toyMaze.obj" if you wish.
Likewise, if you want to see a better implementation of the character fit, you can replace line 204, "obj_trans.translate(0, -1, +2.2);" with "obj_trans.translate(0, 0, +2);" and commenting out lines 374 to 378.
This will have the character camera centered in the taurus we were given for the EC lab, and demonstrates that the character does follow perfectly with the camera (though this can also be accmomplished by just looking down and then moving in the current edition).
