//primary object loading source/info page used http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/
//Code from https://github.com/OpenGLInsights/OpenGLInsightsCode/blob/master/Chapter%2026%20Indexing%20Multiple%20Vertex%20Arrays/common/objloader.cpp
#ifndef OBJLOADER_H
#define OBJLOADER_H

bool loadOBJ(
	const char * path,
	std::vector<glm::vec3> & out_vertices,
	std::vector<glm::vec2> & out_uvs,
	std::vector<glm::vec3> & out_normals
);

#endif